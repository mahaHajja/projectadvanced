package com.example.pizzaapplication_v10;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {

    public static User app_user;

    DataBaseHelper dataBaseHelper = new DataBaseHelper(LoginActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);

        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        String savedEmail = sharedPreferences.getString("email","noValue");
        if(!savedEmail.equals("noValue")){
            ((EditText) findViewById(R.id.editEmail)).setText(savedEmail);
        }

        Button backButton = (Button) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); //Start CustomerMain Activity
                Intent myIntent = new Intent(LoginActivity.this, LoginRegestrationActivity.class);
                LoginActivity.this.startActivity(myIntent);
            }
        });


        Button submitButton = (Button) findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView errorMsg = ((TextView) findViewById(R.id.errorMsg));

                errorMsg.setVisibility(View.GONE);
                String email = ((EditText) findViewById(R.id.editEmail)).getText().toString();
                String password = ((EditText) findViewById(R.id.password)).getText().toString();
                boolean remember = ((CheckBox) findViewById(R.id.checkRemember)).isChecked();

                User user = dataBaseHelper.getUserByEmail(email);
                if (user == null || !PasswordUtils.verifyUserPassword(password, user.getPassword())) {
                    errorMsg.setVisibility(View.VISIBLE);
                } else {
                    if (remember) {
                        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("email", email);
                        editor.commit();
                    }
                    app_user = user;

                    finish(); //Start CustomerMain Activity
                    Intent myIntent = new Intent(LoginActivity.this, NavigationDrawerActivity.class);
                    LoginActivity.this.startActivity(myIntent);
                }
            }
        });
    }
}
