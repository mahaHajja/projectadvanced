package com.example.pizzaapplication_v10;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PizzaDetailsFragment extends Fragment {
    TextView pizzaDetails;
    TextView pizzaName;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pizza_details_fragment, container, false);
        pizzaDetails =view.findViewById(R.id.pizza_details);
        pizzaName =view.findViewById(R.id.pizza_name);
        return view;
    }
    public interface Communicator { public void respond(Pizza pizza); }

    public void changeData(Pizza pizza) {
        if(pizza.name == null || pizza.name.length()==0){
            pizzaName.setText("");
            pizzaDetails.setText("");
        }else{
            pizzaName.setText(pizza.getName());
            pizzaDetails.setText("Type: "+pizza.getType()+"\nPrice:     S: "+pizza.smallPrice+
                    "     M: "+pizza.getMediumPrice() + "     L: "+ pizza.getLargePrice()+"\n\n" +pizza.getSummary());
        }

    }
}