package com.example.pizzaapplication_v10;

import android.app.Activity;
import android.os.AsyncTask;

import org.json.JSONException;

import java.net.HttpURLConnection;
import java.util.List;


class ConnectionAsyncTask extends AsyncTask<String,String,String> {

    Activity activity;

    public ConnectionAsyncTask(Activity activity) {

        this.activity=activity;
    }

    @Override
    protected void onPreExecute() {

//        ((MainActivity)activity).setButtonText("connecting");
        super.onPreExecute();
        ((MainActivity)activity).setProgress(true);
    }

    @Override
    protected String doInBackground(String... params) {

        String data=HttpManager.getData(params[0]);

        return data;
    }

    @Override
    protected void onPostExecute(String s)  {
        super.onPostExecute(s);
        ((MainActivity)activity).setProgress(false);

        ((MainActivity)activity).pizzaList=PizzaJsonParser.getObjectFromJason(activity,s);
        if(((MainActivity)activity).pizzaList != null){
            ((MainActivity)activity).startLoginRegestrationLayout();
        }


    }
}