package com.example.pizzaapplication_v10;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
    public static List<Pizza> pizzaList = new ArrayList<Pizza>();
    Button button;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        setProgress(false);

        button = (Button) findViewById(R.id.getStarted);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ConnectionAsyncTask connectionAsyncTask = new ConnectionAsyncTask(MainActivity.this);
                    connectionAsyncTask.execute("http://www.mocky.io/v2/5b522fa32e000074005c1c40");
                } catch (Exception ex) {
                    showError();
                }

            }
        });


    }

    public void setButtonText(String text) {
        button.setText(text);
    }


    public void setProgress(boolean progress) {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.ProgressBar);
        if (progress == true) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    public void startLoginRegestrationLayout() {
        finish(); //Start CustomerMain Activity
        Intent myIntent=new Intent(MainActivity.this, LoginRegestrationActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    public void showError() {
        Toast toast = new Toast(getApplicationContext());
//        CharSequence text = "An Error Has Occured!";
//        int duration = Toast.LENGTH_LONG;
        View toastView = getLayoutInflater().inflate(R.layout.custom_error_toast, null);
//        Toast toast = Toast.makeText(context, text, duration);
        toast.setView(toastView);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
//        toast.setGravity(Gravity.CENTER| Gravity.CENTER, 0, 0);
        toast.show();
    }
}