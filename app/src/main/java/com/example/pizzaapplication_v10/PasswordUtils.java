package com.example.pizzaapplication_v10;

import android.os.Build;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Random;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class PasswordUtils {
    public static  String salt = "EqdmPh53c9x33EygXpTpcoJvc4VXLK";
    private static final Random RANDOM = new SecureRandom();
    private static final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static final int ITERATIONS = 10000;
    private static final int KEY_LENGTH = 256;

    public static String getSalt(int length) {
        StringBuilder returnValue = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
        }
        return new String(returnValue);
    }
    public static byte[] hash(char[] password, byte[] salt) {
        PBEKeySpec spec = new PBEKeySpec(password, salt, ITERATIONS, KEY_LENGTH);
        Arrays.fill(password, Character.MIN_VALUE);
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            return skf.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                throw new AssertionError("Error while hashing a password: " + e.getMessage(), e);
            }

        } finally {
            spec.clearPassword();
        }
        return null;
    }
    public static String generateSecurePassword(String password, String salt) {
        String returnValue = null;
        byte[] securePassword = hash(password.toCharArray(), salt.getBytes());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            returnValue = Base64.getEncoder().encodeToString(securePassword);
        }

        return returnValue;
    }

//    public static boolean verifyUserPassword(String providedPassword,
//                                             String securedPassword, String salt)
//    {
//        boolean returnValue = false;
//
//        // Generate New secure password with the same salt
//        String newSecurePassword = generateSecurePassword(providedPassword, salt);
//
////        Log.d("sth",securedPassword);
////        Log.d("sth22",newSecurePassword);
//
//        // Check if two passwords are equal
//        returnValue = newSecurePassword.equalsIgnoreCase(securedPassword);
//
//
//        return returnValue;
//    }


    public static boolean verifyUserPassword(String providedPassword,
                                             String securedPassword)
    {
        boolean returnValue = false;

        // Generate New secure password with the same salt
        String newSecurePassword = hashPassword(providedPassword);

//        Log.d("sth",securedPassword);
//        Log.d("sth22",newSecurePassword);

        // Check if two passwords are equal
        returnValue = newSecurePassword.equalsIgnoreCase(securedPassword);


        return returnValue;
    }

//    public static String protectUserPassword(String userPassword){
////        String myPassword = "myPassword123";
////
//        // Generate Salt. The generated value can be stored in DB.
//        String salt = PasswordUtils.getSalt(30);
//
//        // Protect user's password. The generated value can be stored in DB.
//        return PasswordUtils.generateSecurePassword(userPassword, salt);
//    }


    public static String hashPassword(String passwordToHash){
//        String passwordToHash = "password";
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(passwordToHash.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
//        System.out.println(generatedPassword);
        return generatedPassword;
    }



}