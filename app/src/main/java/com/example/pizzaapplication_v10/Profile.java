package com.example.pizzaapplication_v10;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class Profile extends Fragment {
    static boolean change=false;
    static TextView firstName;
    static TextView lastName;
    static TextView phone;
    static User app_user_pointer;
    static User app_user;
    DataBaseHelper dataBaseHelper ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view =  inflater.inflate(R.layout.fragment_profile, container, false);

        dataBaseHelper = new DataBaseHelper(view.getContext());

        app_user_pointer = LoginActivity.app_user;
        app_user=new User(app_user_pointer.getId(),app_user_pointer.getEmail(),app_user_pointer.getPassword(),
                app_user_pointer.getFirstName(), app_user_pointer.getLastName(),app_user_pointer.getPhoneNumber(),
                app_user_pointer.getGender(),0);

        ImageView profilePic = (ImageView) view.findViewById(R.id.profilePic);
        ImageView editFirstName = (ImageView) view.findViewById(R.id.editFirstName);
        ImageView editLastName = (ImageView) view.findViewById(R.id.editLastName);
        ImageView editPhone = (ImageView) view.findViewById(R.id.editPhone);
        firstName = (TextView) view.findViewById(R.id.firstName);
        lastName = (TextView) view.findViewById(R.id.lastName);
        phone = (TextView) view.findViewById(R.id.phone);
        TextView email = (TextView) view.findViewById(R.id.email);

        //init

        firstName.setText(app_user.getFirstName());
        lastName.setText(app_user.getLastName());
        phone.setText(app_user.getPhoneNumber());
        email.setText(app_user.getEmail());

        editFirstName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent=new Intent(getActivity(), EditPopUp.class);
                String title="Edit First Name";
                String txt="New Name: ";
                String hint="Enter first name..";
                myIntent.putExtra("txt",txt);
                myIntent.putExtra("title",title);
                myIntent.putExtra("hint",hint);
                myIntent.putExtra("mode",1);
                startActivity(myIntent);
            }
        });
        editLastName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent=new Intent(getActivity(), EditPopUp.class);
                String title="Edit Last Name";
                String txt="New Name: ";
                String hint="Enter last name..";
                myIntent.putExtra("txt",txt);
                myIntent.putExtra("title",title);
                myIntent.putExtra("hint",hint);
                myIntent.putExtra("mode",2);

                startActivity(myIntent);
            }
        });
        editPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent=new Intent(getActivity(), EditPopUp.class);
                String title="Edit Phone Number";
                String txt="New Phone: ";
                String hint="Enter Phone Number..";
                myIntent.putExtra("txt",txt);
                myIntent.putExtra("title",title);
                myIntent.putExtra("hint",hint);
                myIntent.putExtra("mode",3);

                startActivity(myIntent);
            }
        });

        Button popup=(Button) view.findViewById(R.id.pop1);
        popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ChangePassPopUp.class));
            }
        });

        Button save=(Button) view.findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(change) {
                    dataBaseHelper.updateUserInfo(app_user);
                }
                String msg=(change)?"Changes Saved":"No Changes To Save";
                Toast toast=Toast.makeText(view.getContext(),msg,Toast.LENGTH_SHORT);
                toast.show();

                change=false;
            }
        });

        return view;
    }
    public static void update() {
        firstName.setText(app_user.getFirstName());
        lastName.setText(app_user.getLastName());
        phone.setText(app_user.getPhoneNumber());
    }

}
