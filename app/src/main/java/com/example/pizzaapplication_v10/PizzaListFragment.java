package com.example.pizzaapplication_v10;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class PizzaListFragment extends Fragment {
    LinearLayout linearLayout;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pizza_list_fragment, container, false);

        String[] options = {"Any","veggies", "beef", "chicken", "others"};
        final Spinner spnType = (Spinner) view.findViewById(R.id.Type);
        ArrayAdapter objTypeArr = new ArrayAdapter(view.getContext(), android.R.layout.simple_spinner_item, options);
        spnType.setAdapter(objTypeArr);

        String[] options2 = {"Any", "Small", "Medium", "Large"};
        final Spinner spnSize = (Spinner) view.findViewById(R.id.Size);
        ArrayAdapter objSizeArr = new ArrayAdapter(view.getContext(), android.R.layout.simple_spinner_item, options2);
        spnSize.setAdapter(objSizeArr);


        final PizzaDetailsFragment.Communicator communicator = (PizzaDetailsFragment.Communicator) getParentFragment();
        final List<Pizza> pizzaList = MainActivity.pizzaList;

        fillView(communicator, pizzaList);

        ImageView searchBtn = (ImageView) view.findViewById(R.id.search);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = ((Spinner) view.findViewById(R.id.Type)).getSelectedItem().toString();
                String price = ((EditText) view.findViewById(R.id.Price)).getText().toString() + "$";
                String size = ((Spinner) view.findViewById(R.id.Size)).getSelectedItem().toString();

                List<Pizza> filteredList = new ArrayList<Pizza>();
                for (int j = 0; j < pizzaList.size(); j++) {
                    if ((type.equals("Any") || pizzaList.get(j).type.equals(type))
                            && (isPriceEqual(pizzaList.get(j),size,price)|| price.length()==1)) {
                        filteredList.add(pizzaList.get(j));
                    }
                }
                Context context = view.getContext();
                CharSequence text = filteredList.size()+" ";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                fillView(communicator, filteredList);
            }
        });


        return view;
    }

    public boolean isPriceEqual(Pizza pizza, String size, String price) {
        if (size.equals("Any")) {
            if (pizza.getLargePrice().equals(price) ||
                    pizza.getSmallPrice().equals(price) ||
                    pizza.getMediumPrice().equals(price)) {
                return true;
            }
        } else if (size == "Medium") {
            if (pizza.getMediumPrice().equals(price)) {
                return true;
            }
        } else if (size == "Large") {
            if (pizza.getLargePrice().equals(price)) {
                return true;
            }
        } else {
            if (pizza.getSmallPrice().equals(price)) {
                return true;
            }
        }
        return false;
    }

    public void fillView(final PizzaDetailsFragment.Communicator communicator, final List<Pizza> pizzaList) {
        linearLayout = (LinearLayout) view.findViewById(R.id.list_layout);
        linearLayout.removeAllViews();
        for (int i = 0; i < pizzaList.size(); i++) {
            LinearLayout item = new LinearLayout(view.getContext());
            item.setOrientation(LinearLayout.HORIZONTAL);
            item.setGravity(Gravity.CENTER_VERTICAL);
            item.setBackgroundColor(Color.parseColor("#9C0E0D"));
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(10, 0, 10, 20);
            item.setLayoutParams(lp);
            item.setPadding(15, 15, 15, 15);

            final TextView text = new TextView(view.getContext());
            text.setText(pizzaList.get(i).name + "\n");
            text.setTextColor(Color.parseColor("#FFFFFF"));
            text.setTextSize(16);
            LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
            lp2.setMargins(10, 10, 10, 10);
            text.setLayoutParams(lp2);
            text.setGravity(Gravity.CENTER_VERTICAL);
            text.getLayoutParams().width = 650;
            text.setId(i);
            item.addView(text);


            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    communicator.respond(pizzaList.get(v.getId()));
                }
            });

            ImageView favourite = new ImageView(view.getContext());
            favourite.setImageResource(R.drawable.favourate_foreground);
            favourite.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            favourite.getLayoutParams().height = 70;
            favourite.getLayoutParams().width = 70;


            item.addView(favourite);

            linearLayout.addView(item);
        }
    }
}
