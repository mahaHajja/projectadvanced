package com.example.pizzaapplication_v10;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class LoginRegestrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login_regestration);


       Button signupButton = (Button) findViewById(R.id.signupButton);
       signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); //Start CustomerMain Activity
                Intent myIntent = new Intent(LoginRegestrationActivity.this,SignupActivity.class);
                LoginRegestrationActivity.this.startActivity(myIntent);
            }
        });

        Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); //Start CustomerMain Activity
                Intent myIntent = new Intent(LoginRegestrationActivity.this,LoginActivity.class);
                LoginRegestrationActivity.this.startActivity(myIntent);
            }
        });
    }
}
