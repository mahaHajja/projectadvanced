package com.example.pizzaapplication_v10;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.regex.Pattern;

public class SignupActivity extends AppCompatActivity {

    DataBaseHelper dataBaseHelper = new DataBaseHelper(SignupActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_signup);

        String[] options = {"Male", "Female"};
        final Spinner spnGender = (Spinner) findViewById(R.id.genderSpinner);//simple_spinner_item
        ArrayAdapter objGenderArr = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, options);
        spnGender.setAdapter(objGenderArr);

        User user = new User();
        user.setFirstName("Maha");
        user.setLastName("Hajja");
        user.setPassword("m123456789");
        user.setPhoneNumber("0595591190");
        user.setEmail("maha_durgham@yahoo.com");
        user.setAdmin(1);
        user.setGender(0);
        if(dataBaseHelper.getUserByEmail("maha_durgham@yahoo.com") == null){
            dataBaseHelper.insertUser(user);
        }

        Button backButton = (Button) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); //Start CustomerMain Activity
                Intent myIntent = new Intent(SignupActivity.this, LoginRegestrationActivity.class);
                SignupActivity.this.startActivity(myIntent);
            }
        });


        Button signupButton = (Button) findViewById(R.id.submitButton);
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean error = false;
                EditText firstName = (EditText) findViewById(R.id.editFirstName);
                EditText lastName = (EditText) findViewById(R.id.editLastName);
                EditText email = (EditText) findViewById(R.id.editEmail);
                EditText phone = (EditText) findViewById(R.id.editPhone);
                EditText password1 = (EditText) findViewById(R.id.password1);
                EditText password2 = (EditText) findViewById(R.id.password2);
                String gender = spnGender.getSelectedItem().toString();

                String stFirstName = firstName.getText().toString();
                String stLastName = lastName.getText().toString();
                String stEmail = email.getText().toString();
                String stPhone = phone.getText().toString();
                String stPassword1 = password1.getText().toString();
                String stPassword2 = password2.getText().toString();


                //validation
                if (stFirstName.length() < 3){
                    firstName.setText("");
                    firstName.setHintTextColor(Color.rgb(237,131,112));
                    firstName.setHint("Must be at least 3 letters");
                    error = true;
                }
                if(stLastName.length() < 3){
                    lastName.setText("");
                    lastName.setHintTextColor(Color.rgb(237,131,112));
                    lastName.setHint("Must be at least 3 letters");
                    error = true;
                }

                if(stPhone.length() < 10){
                    phone.setText("");
                    phone.setHintTextColor(Color.rgb(237,131,112));
                    phone.setHint("Must be 10 digits");
                    error = true;
                }else if(!stPhone.substring(0,2).equals("05")){
                    phone.setText("");
                    phone.setHintTextColor(Color.rgb(237,131,112));
                    phone.setHint("Must start with '05'");
                    error = true;
                }
                if(stPassword1.length() < 8){
                    password1.setText("");
                    password1.setHintTextColor(Color.rgb(237,131,112));
                    password1.setHint("Must be at least 8 characters");
                    error = true;
                }else if(!Pattern.compile( "[0-9]" ).matcher( stPassword1 ).find()){
                    password1.setText("");
                    password1.setHintTextColor(Color.rgb(237,131,112));
                    password1.setHint("Must contain a digit");
                    error = true;
                }else if(!Pattern.compile( "[a-z]" ).matcher( stPassword1.toLowerCase() ).find()){
                    password1.setText("");
                    password1.setHintTextColor(Color.rgb(237,131,112));
                    password1.setHint("Must contain a letter");
                    error = true;
                }
                if(!stPassword1.equals(stPassword2)){
                    password2.setText("");
                    password2.setHintTextColor(Color.rgb(237,131,112));
                    password2.setHint("Passwords doesn't match!");
                    error = true;
                }

                Pattern pattern =  Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
                if(! pattern.matcher(stEmail).find()){
                    email.setText("");
                    email.setHintTextColor(Color.rgb(237,131,112));
                    email.setHint("Invalid Email!");
                    error = true;
                }else if(dataBaseHelper.getUserByEmail(stEmail) != null){
                    email.setText("");
                    email.setHintTextColor(Color.rgb(237,131,112));
                    email.setHint("This email is used");
                    error = true;
                }



                if(!error){
                    User user=new User(0,stEmail,stPassword1,stFirstName,stLastName,stPhone,(gender.equals("male")?1:0),0);
                    dataBaseHelper.insertUser(user);
                    finish(); //Start CustomerMain Activity
                    Intent myIntent = new Intent(SignupActivity.this, LoginActivity.class);
                    SignupActivity.this.startActivity(myIntent);
                }
            }
        });

       // TextView textView = (TextView) findViewById(R.id.textView);

//        cursor = dataBaseHelper.getAllCustomers();
//        while (cursor.moveToNext())
//            textView.setText(cursor.getString(0) + "\n" + cursor.getString(1) + "\n" + cursor.getString(2) + "\n" + cursor.getString(3));
//
    }
}
