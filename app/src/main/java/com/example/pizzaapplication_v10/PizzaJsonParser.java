package com.example.pizzaapplication_v10;

import android.app.Activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mohammad on 7/30/2017.
 */

public class PizzaJsonParser {

    public static List<Pizza> getObjectFromJason(Activity activity,String jason) {
        List<Pizza> pizzaList;
        try {
            JSONArray jsonArray = new JSONArray(jason);
            pizzaList = new ArrayList<>();
            for(int i = 0; i<jsonArray.length(); i++)
            {
                JSONObject jsonObject = new JSONObject();
                jsonObject= (JSONObject) jsonArray.get(i);
                Pizza pizza = new Pizza();
                pizza.setName(jsonObject.getString("name"));
                pizza.setSummary(jsonObject.getString("summary"));
                pizza.setOffer(jsonObject.getString("offer"));
                pizza.setType(jsonObject.getString("type"));
                pizza.setLargePrice(jsonObject.getString("largePrice"));
                pizza.setSmallPrice(jsonObject.getString("smallPrice"));
                pizza.setMediumPrice(jsonObject.getString("medium"));


                pizzaList.add(pizza);
            }
        } catch (Exception e)
        { e.printStackTrace();
            ((MainActivity)activity).showError();
            return null;
        }
        return pizzaList;
    }
}