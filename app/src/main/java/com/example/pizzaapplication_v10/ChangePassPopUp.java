package com.example.pizzaapplication_v10;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Pattern;

public class ChangePassPopUp extends AppCompatActivity {

    static EditText password1;
    static EditText password2;
    DataBaseHelper dataBaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_change_pass_pop_up);

        dataBaseHelper = new DataBaseHelper(ChangePassPopUp.this);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width= dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width*0.9),(int)(height*0.4));

        password1=(EditText) findViewById(R.id.password);
        password2=(EditText) findViewById(R.id.confirm);

        Button ok= (Button) findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean flag = validate();
                if(flag) {
                    dataBaseHelper.changePass(password1.getText().toString());
                    Toast toast = Toast.makeText(getApplicationContext(), "Password Saved", Toast.LENGTH_SHORT);
                    toast.show();
                    finish();
                }
            }
        });

        Button back= (Button) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public boolean validate() {
        String stPassword1=password1.getText().toString();
        String stPassword2=password2.getText().toString();

        if(stPassword1.length() < 8){
            password1.setText("");
            password1.setHintTextColor(Color.rgb(237,0,0));
            password1.setHint("Must be at least 8 characters");
            return false;
        }else if(!Pattern.compile( "[0-9]" ).matcher( stPassword1 ).find()){
            password1.setText("");
            password1.setHintTextColor(Color.rgb(237,0,0));
            password1.setHint("Must contain a digit");
            return false;
        }else if(!Pattern.compile( "[a-z]" ).matcher( stPassword1.toLowerCase() ).find()){
            password1.setText("");
            password1.setHintTextColor(Color.rgb(237,0,0));
            password1.setHint("Must contain a letter");
            return false;
        }
        if(!stPassword1.equals(stPassword2)){
            password2.setText("");
            password2.setHintTextColor(Color.rgb(237,0,0));
            password2.setHint("Doesn't match!");
            return false;
        }
        return true;
    }
}
