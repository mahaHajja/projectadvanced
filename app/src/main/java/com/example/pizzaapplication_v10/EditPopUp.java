package com.example.pizzaapplication_v10;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EditPopUp extends AppCompatActivity {
    static EditText editText;
    static int mode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_edit_pop_up);


        TextView txt=(TextView) findViewById(R.id.txt);
         editText=(EditText) findViewById(R.id.editText);
        Intent intent=getIntent();
        txt.setText(intent.getStringExtra("txt"));
        String hint = intent.getStringExtra("hint");
         mode = intent.getIntExtra("mode",1);
        editText.setHint(hint);



        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width= dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width*0.9),(int)(height*0.4));

        Button ok= (Button) findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean flag=validate(mode);
                if(flag) {
                    changeProfileUser(mode);
                    Profile.update();
                    finish();
                }
            }
        });

        Button back= (Button) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



    }
    public boolean validate(int mode) {

        String input= editText.getText().toString();
        if(mode==1 || mode==2){
            if (input.length() < 3){
                editText.setText("");
                editText.setHintTextColor(Color.rgb(237,0,0));
                editText.setHint("At least 3 letters");
                return false;
            }
        }else{
            if(input.length() < 10){
                editText.setText("");
                editText.setHintTextColor(Color.rgb(237,0,0));
                editText.setHint("Must be 10 digits");
                return false;
            }else if(!input.substring(0,2).equals("05")){
                editText.setText("");
                editText.setHintTextColor(Color.rgb(237,0,0));
                editText.setHint("Must start with '05'");
                return false;
            }
        }
        return true;
    }

    public void changeProfileUser(int mode){
        String before;
        String after;
        after= editText.getText().toString();
        if(mode==1){
            before = LoginActivity.app_user.getFirstName();

            if(!before.equals(after)){
                Profile.change=true;
                Profile.app_user.setFirstName(after);
            }


        }else if(mode==2){
            before = LoginActivity.app_user.getLastName();

            if(!before.equals(after)){
                Profile.change=true;
                Profile.app_user.setLastName(after);
            }


        }else if(mode==3){
            before = LoginActivity.app_user.getPhoneNumber();

            if(!before.equals(after)){
                Profile.change=true;
                Profile.app_user.setPhoneNumber(after);
            }


        }
    }
}
