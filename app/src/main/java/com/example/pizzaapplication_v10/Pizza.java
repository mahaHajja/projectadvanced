package com.example.pizzaapplication_v10;

public class Pizza {
    String name;
    String summary;
    String type;
    String offer;
    String smallPrice;
    String mediumPrice;
    String largePrice;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getSmallPrice() {
        return smallPrice;
    }

    public void setSmallPrice(String smallPrice) {
        this.smallPrice = smallPrice;
    }

    public String getMediumPrice() {
        return mediumPrice;
    }

    public void setMediumPrice(String mediumPrice) {
        this.mediumPrice = mediumPrice;
    }

    public String getLargePrice() {
        return largePrice;
    }

    public void setLargePrice(String largePrice) {
        this.largePrice = largePrice;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", summary='" + summary + '\'' +
                ", type='" + type + '\'' +
                ", offer='" + offer + '\'' +
                ", smallPrice='" + smallPrice + '\'' +
                ", mediumPrice='" + mediumPrice + '\'' +
                ", largePrice='" + largePrice + '\'' +
                '}';
    }
}
