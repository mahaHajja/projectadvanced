package com.example.pizzaapplication_v10;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DataBaseHelper extends SQLiteOpenHelper {
    public DataBaseHelper(Context context) {
        super(context, "PIZZA_DB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE USERS(USER_ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "FIRST_NAME TEXT, " +
                "LAST_NAME TEXT, " +
                "EMAIL TEXT UNIQUE, " +
                "PASSWORD TEXT, " +
                "GENDER INTEGER, " +
                "PHONE_NUMBER TEXT, " +
                "IS_ADMIN INTEGER)");
        db.execSQL("CREATE TABLE ORDERS(ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "PIZZA_INDEX INTEGER, " +
                "PIZZA_SIZE INTERGER, " +
                "TIMESTAMP DATETIME DEFAULT CURRENT_TIMESTAMP, " +
                "USER_ID INTEGER, "+
                "FOREIGN KEY (USER_ID) REFERENCES USERS(USER_ID))");
        db.execSQL("CREATE TABLE FAVOURATES(ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "PIZZA_INDEX INTEGER, " +
                "USER_ID INTEGER, "+
                "FOREIGN KEY (USER_ID) REFERENCES USERS(USER_ID))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void insertUser(User user) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("FIRST_NAME", user.getFirstName());
        contentValues.put("LAST_NAME", user.getLastName());
        contentValues.put("EMAIL", user.getEmail());
        contentValues.put("PHONE_NUMBER", user.getPhoneNumber());
        contentValues.put("GENDER", user.getGender());
        contentValues.put("PASSWORD", user.getPassword());
        contentValues.put("IS_ADMIN", 0);
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.insert("USERS", null, contentValues);
    }

    public Cursor getAllCustomers() {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM USERS", null);
        return cursor;
    }
    public User getUserByEmail(String email){
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM USERS WHERE EMAIL = \""+ email+"\"", null);
        List<User> users = cursorToUser(cursor);
        if(users.isEmpty()){
            return null;
        }else{
            return users.get(0);
        }

    }

    public List<User> cursorToUser(Cursor cursor){
//        db.execSQL("CREATE TABLE USERS(USER_ID INTEGER PRIMARY KEY AUTOINCREMENT," +
//                "FIRST_NAME TEXT, " +
//                "LAST_NAME TEXT, " +
//                "EMAIL TEXT, " +
//                "PASSWORD TEXT, " +
//                "GENDER INTEGER, " +
//                "PHONE_NUMBER TEXT, " +
//                "IS_ADMIN INTEGER)");
        List<User> users =  new ArrayList<User>();
        while (cursor.moveToNext()){
            User user =  new User();
            user.setFirstName(cursor.getString(1));
            user.setLastName(cursor.getString(2));
            user.setEmail(cursor.getString(3));
            user.password = cursor.getString(4);
            user.setGender(cursor.getInt(5));
            user.setPhoneNumber(cursor.getString(6));
            user.setAdmin(cursor.getInt(7));
            user.setId(cursor.getInt(0));
            users.add(user);
        }

        return users;
    }

    public void AddFavouriteToUser(long user_id, String pizza_name) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("PIZZA_NAME", pizza_name);
        contentValues.put("USER_ID", user_id);
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.insert("FAVOURATES", null, contentValues);
    }

    public void updateUserInfo(User user_pointer) {

//        User user_pointer = LoginActivity.app_user;
        String fn=user_pointer.getFirstName();
        String ln=user_pointer.getLastName();
        String phone=user_pointer.getPhoneNumber();
//        String pass=user_pointer.getPassword();
        long id = user_pointer.getId();


        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL("UPDATE USERS SET FIRST_NAME='"+fn+"' , " +
                "LAST_NAME='"+ln+"' , " +
                "PHONE_NUMBER='"+phone+"'  " +
//                " ,PASSWORD='"+pass+"'  "+
                " WHERE USER_ID=" + id );

        User pointer = LoginActivity.app_user;
        pointer.setFirstName(fn);
        pointer.setLastName(ln);
        pointer.setPhoneNumber(phone);
//        pointer.setPassword(pass);

    }

    public void changePass(String pass) {

        long id = LoginActivity.app_user.getId();
        String hash=PasswordUtils.hashPassword(pass);

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL("UPDATE USERS SET PASSWORD='"+hash+"'" +
                " WHERE USER_ID=" + id );

        LoginActivity.app_user.setPassword(pass);

    }



}
