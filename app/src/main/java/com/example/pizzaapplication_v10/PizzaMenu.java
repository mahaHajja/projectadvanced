package com.example.pizzaapplication_v10;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class PizzaMenu extends Fragment implements PizzaDetailsFragment.Communicator{
    TextView txt;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_pizza_menu, container, false);


//        Button btn = (Button) view.findViewById(R.id.button);
//        txt = (TextView) view.findViewById(R.id.txt);
////        btn.setText("iiii");
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                txt.setText("OOOOOOOOOOOOOOOOO");
//            }
//        });

        return view;
    }

    @Override
    public void respond(Pizza pizza) {
        PizzaDetailsFragment fragment = (PizzaDetailsFragment)getChildFragmentManager().findFragmentById(R.id.pizza_details_fragment) ;
        fragment.changeData(pizza);

    }
}